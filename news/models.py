from django.db import models
class Articles(models.Model):
    title = models.CharField('title', max_length=50)
    anons = models.CharField('anons', max_length=250)
    full_text = models.TextField('statya')
    date = models.DateTimeField('data publicatioin')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Articles'
        verbose_name_plural = 'Articles'